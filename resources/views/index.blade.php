<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</head>

<body>
    <h1 class="text-center">CRUD DATA MAHASISWA</h1>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-end me-2 mb-2">
                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalInput">Input
                    Data</button>
            </div>
        </div>
    </div>

    {{-- Modal Input Data --}}
    <div class="modal fade" id="modalInput" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="staticBackdropLabel">Form Tambah Data Mahasiswa</h5>
                </div>
                <div class="modal-body">
                    <form id="tambahData">
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="inputNama" name="Nama" required>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="inputNIM" class="col-sm-4 col-form-label">NIM</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="inputNIM" name="NIM" required>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="inputJK" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-5">
                                <select class="form-select" id="inputGroupSelect01">
                                    <option selected value="1">Laki-laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mt-3">
                            <label for="inputProdi" class="col-sm-4 col-form-label">Prodi</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="inputProdi" name="Prodi" required>
                            </div>
                        </div>
                        <div class="form-group row mt-3 mb-2">
                            <label for="inputFakultas" class="col-sm-4 col-form-label">Fakultas</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="inputFakultas" name="Fakultas" required>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-success" id="buttonTambahData">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jenis Kelamin</th>
            <th>Prodi</th>
            <th>Fakultas</th>
            <th>Edit</th>
            <th>Hapus</th>
        </tr>
        <tr>

        </tr>
    </table>
    <button type="button" class="btn btn-danger float-end me-2 mb-2" data-bs-toggle="modal"
        data-bs-target="#modalHapusSemua">Hapus Semua</button>

    {{-- Modal Hapus Semua --}}
    <div class="modal fade" id="modalHapusSemua" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Peringatan!</h5>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin ingin menghapus semua data?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-danger" id="buttonHapusSemua">Yakin</button>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
